require("@babel/polyfill");

import * as riot from "riot";
import main from "./main.riot";

import "./index.css";
// import { render, html, svg } from "uhtml";
// import payments from "./../templates/payments.html";

export class Sprinque {
  publicKey: string;
  constructor({ publicKey }: { publicKey: string }) {
    this.publicKey = publicKey;
  }
  async createCheckout() {
    const data = {
      number: "34244433",
      po_number: "fofe94023",
      currancy: "USD",
      amount: "34050",
    };
    fetch("localhost:3000/invoice", {
      method: "post",
      body: JSON.stringify(data),
    }).then(() => {
      this.createModal();
    });
  }
  private createModal() {
    const div = document.createElement("div");
    document.querySelector("body").appendChild(div);
    const createMain = riot.component(main);
    createMain(div);
  }
}

window.Sprinque = Sprinque;
global.Sprinque = Sprinque;

export default Sprinque;
